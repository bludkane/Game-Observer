
USE operate_dol;

DELETE FROM `dol_runtime_data`;

DELIMITER $$
DROP PROCEDURE IF EXISTS `autoInsert`$$

CREATE PROCEDURE `autoInsert`()
BEGIN
    DECLARE i int default 0;
    WHILE(i < 288) DO
        INSERT INTO `dol_runtime_data` (`now_time`) VALUES ( from_unixtime(1430424000+i*60*5) );
        SET i = i+1;
    END WHILE;
    END$$

DELIMITER ;

call autoInsert();

DROP PROCEDURE IF EXISTS `autoInsert`
