USE operate_dol;

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  `passwd` VARCHAR(128) NOT NULL,
  `role` TINYINT(4) NOT NULL,
  `state` TINYINT(4) NOT NULL,
  `create_time` DATETIME NOT NULL DEFAULT NOW(),
  `last_login` DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `admin` (`name`, `passwd`, `role`, `state`) VALUES ('admin','admin',1,1);

-- -----------------------------------------------------
-- 同时在线统计
-- -----------------------------------------------------
DROP TABLE IF EXISTS `runtime_data`;
CREATE TABLE `runtime_data` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `now_time` TIME NOT NULL,
  `active_user` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '同时在线用户数',
  `active_new_user` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '同时在线新增用户数',
  `active_pay_user` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '同时在线付费用户数',
  `update_time` DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`),
  INDEX `now_time` (`now_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1;

-- -----------------------------------------------------
-- 每日统计
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_data`;
CREATE TABLE `user_data` (
`user_id` INT UNSIGNED NOT NULL, 
`platform_id` INT UNSIGNED NOT NULL, 
`first_login_date` DATETIME NOT NULL DEFAULT NOW() COMMENT '第一次登陆游戏时间', 
`last_login_date` DATETIME NOT NULL DEFAULT NOW() COMMENT '最后一次登陆游戏时间', 
`last_update_date` DATETIME NOT NULL DEFAULT NOW() COMMENT '最后一次更新时间', 
`online_amount_time` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT '在线时长总计', 
`online_max_time` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT '最长一次在线时长', 
`login_amount` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT '登陆次数', 
`connect_amount` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT '连接次数', 
`new_flag` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否新增', 
`pay_flag` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否付费', 
PRIMARY KEY (`user_id`),
INDEX `platform_id` (`platform_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8; 

-- -----------------------------------------------------
-- 每个用户累积统计
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_data_by_user`;
CREATE TABLE `user_data_by_user` (
  `user_id` INT UNSIGNED NOT NULL,
  `platform_id` INT UNSIGNED NOT NULL,
  `first_login_date` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '第一次登陆游戏时间',
  `last_update_date` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后一次登陆游戏时间',
  `online_amount_time` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '在线时长总计',
  `online_max_time` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '最长一次在线时长',
  `login_amount` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '登陆次数',
  `connect_amount` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '连接次数',
  `day_amount` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '登陆天数',
  `pay_flag` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否付费',
  PRIMARY KEY (`user_id`),
  INDEX `platform_id` (`platform_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- 每日统计
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_data_by_date`;
CREATE TABLE `user_data_by_date` (
  `date` DATETIME NOT NULL,
  `new_user` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '新增用户人数',
  `new_pay_user` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '新增付费用户人数',
  `active_user` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '活跃用户人数',
  `active_pay_user` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '活跃付费用户人数',
  `active_user_online_time` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '活跃用户总在线时长',
  `active_user_login_amount` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '活跃用户总登陆次数',
  `active_pay_user_online_time` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '活跃付费用户总在线时长',
  `active_pay_user_login_amount` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '活跃付费用户总登陆次数',
  `active_user_max` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '同时在线用户最大数',
  `active_new_user_max` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '同时在线新增用户最大数',
  `active_pay_user_max` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '同时在线付费用户最大数',
  `one_sesstion_user` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '3次以下有效通信用户数',
  `valid_user` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '有效通讯3次以上人数',
  `valid_user_day_3` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '累计登陆至少2天及有效通讯6次以上人数',
  `valid_user_day_7` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '累计登陆至少3天及有效通讯9次以上人数',
  `valid_user_day_14` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '累计登陆至少5天及有效通讯20次以上人数',
  `valid_user_day_30` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '累计登陆至少10天及有效通讯40次以上人数',
  `day_continue_2` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '继续统计，新增用户第2日登陆人数',
  `day_continue_3` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '继续统计，新增用户第3日登陆人数',
  `day_continue_4` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '继续统计，新增用户第4日登陆人数',
  `day_continue_5` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '继续统计，新增用户第5日登陆人数',
  `day_continue_6` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '继续统计，新增用户第6日登陆人数',
  `day_continue_7` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '继续统计，新增用户第7日登陆人数',
  `day_continue_14` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '继续统计，新增用户第14日登陆人数',
  `day_continue_30` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '继续统计，新增用户第30日登陆人数',
  `day_outflow_7` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '流失统计，7天前登陆过的并且今日未登录用户数',
  `day_outflow_14` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '流失统计，14天前登陆过并且今日未登录用户数',
  `day_outflow_30` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '流失统计，30天前登陆过并且今日未登录用户数',
  `day_return_7` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '回归统计，6天前未登陆过的并且今日登录用户数',
  `day_return_14` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '回归统计，13天前未登陆过并且今日登录用户数',
  `day_return_30` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '回归统计，29天前未登陆过并且今日登录用户数',
  PRIMARY KEY (`date`),
  INDEX `date_platform` (`date`, `platform_id`),
  INDEX `platform_id` (`platform_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

