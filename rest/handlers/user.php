<?php

class user extends userdb
{
    public function create( $user_id, $platform_id )
    {
        //Auth::CheckPermission( ROLE_ADMIN );
        return parent::create( $user_id, $platform_id );
    }

    public function connect( $user_id, $platform_id )
    {
        return parent::connect( $user_id, $platform_id );
    }

    public function pay( $user_id, $platform_id )
    {
        return parent::pay( $user_id, $platform_id );
    }

}

?>