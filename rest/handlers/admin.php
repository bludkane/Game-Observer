<?php

class admin extends admindb {
    public $table_name = "admin";

    public function add( $uname, $passwd ) {
        Auth::CheckPermission( ROLE_ADMIN );
        return parent::add( array (
                'name' => $uname,
                'passwd' => $passwd 
        ) );
    }

    public function find_one( $uname ) {
        Auth::CheckPermission( ROLE_USER );
        return parent::find_one( array (
                'name' => $uname 
        ) );
    }

    public function test() {
        return new response( array (
                'body' => "hello test!!",
                'cache' => '5' 
        ) );
    }

    public function test2() {
        return new response( array (
                'body' => "hello test!!",
                'cache' => 'aa' 
        ) );
    }

    public function login( $uname, $passwd ) {
        setcookie( 'ticket', Auth::GenTicket( $uname, ROLE_USER, Auth::GetSeed() ), time() + 60 * 60 );
        return new response( array (
                'body' => "login",
                'cache' => 'aa' 
        ) );
    }

}

?>