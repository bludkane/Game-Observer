<?php

function get_Ymd()
{
    $date = date( "Y_m_d" );
    $hour = date( "H" );
    if ( $hour < DATE_START_CLOCK ) {
        $date = date( "Y_m_d", strtotime( "-1 day" ) );
    }
    return $date;
}

function get_yestoday_Ymd()
{
    $date = date( "Y_m_d" );
    $hour = date( "H" );
    if ( $hour < DATE_START_CLOCK ) {
        $date = date( "Y_m_d", strtotime( "-2 day" ) );
    }
    return $date;
}

function get_runtime_data_id()
{
    $hour = date( "H" );
    $min = date( "i" );
    if ( $hour < DATE_START_CLOCK ) {
        $hour += 24;
    }
    $hour = $hour - DATE_START_CLOCK;
    $min = $min / 5 + 1;
    $id = $hour * 12 + $min;
    
    return $id;
}