<?php
define( 'LOG_TYPE_INFO', 'INFO' );
define( 'LOG_TYPE_WARN', 'WARN' );
define( 'LOG_TYPE_ERROR', 'ERROR' );
define( 'LOG_TYPE_DEBUG', 'DEBUG' );

class Log {

    private function __construct() {
    }

    public static function I( $message ) {
        $message = static::Write( LOG_TYPE_INFO, $message );
    }

    public static function W( $message ) {
        $message = static::Write( LOG_TYPE_WARN, $message );
    }

    public static function E( $message ) {
        $message = static::Write( LOG_TYPE_ERROR, $message );
    }

    public static function D( $message ) {
        $message = static::Write( LOG_TYPE_DEBUG, $message );
    }
    
    // write a log
    protected static function Write( $type, $message ) {
        $message = static::Format( $type, $message );
        
        // 这里最好注入路径，解耦
        file_put_contents( LOG_PATH, $message, LOCK_EX | FILE_APPEND );
    }
    
    // format a log message
    protected static function Format( $type, $message ) {
        return '[' . date( 'Y-m-d H:i:s' ) . '] ' . strtoupper( $type ) . " - {$message}" . PHP_EOL;
    }
    
    // handle non-existed static method
    public static function __callStatic( $method, $parameters ) {
        static::Write( $method, $parameters[ 0 ] );
    }

}
