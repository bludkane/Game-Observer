<?php
/*
 * 5分钟执行一次脚本。
 * 统计在线情况。
 */
require_once ( "../config.php" );
require_once ( DBASE_PATH . '/dbase.php' );
require_once ( PHPROOT . "/inc/Log.php" );

Log::I( '==== statistics_active_per_5minute ====' );
Log::I( '<<<<<< START >>>>>>' );

$from_table_name = 'user_data_';

$to_table_name = 'runtime_data';

global $g_config;

if ( !isset($g_config[ 'pre' ]) ) {
    Log::E( 'g_config is not exist.' );
    exit();
}

$from_table_name = $g_config[ 'pre' ] . $from_table_name;
$from_table_name .= get_Ymd();

$to_table_name = $g_config[ 'pre' ] . $to_table_name;

$db = new Database();
$db->connect();

// 在最近5分钟内登陆的人数。
$sql = 'SELECT COUNT(user_id) as active_user FROM ' . $from_table_name . " WHERE ( NOW() - last_update_date ) < 300; ";
$obj = $db->fetch_array( $sql );
$active_user = $obj[ 'active_user' ];

// 在最近5分钟内登陆的新增玩家人数。
$sql = 'SELECT COUNT(user_id) as active_new_user FROM ' . $from_table_name . " WHERE ( NOW() - last_update_date ) < 300 and new_flag = 1; ";
$obj = $db->fetch_array( $sql );
$active_new_user = $obj[ 'active_new_user' ];

// 在最近5分钟内登陆的付费玩家人数。
$sql = 'SELECT COUNT(user_id) as active_pay_user FROM ' . $from_table_name . " WHERE ( NOW() - last_update_date ) < 300 and pay_flag = 1; ";
$obj = $db->fetch_array( $sql );
$active_pay_user = $obj[ 'active_pay_user' ];

$data = array (
        'active_user' => $active_user,
        'active_new_user' => $active_new_user,
        'active_pay_user' => $active_pay_user
);

$id = get_runtime_data_id();
$where = " id = $id";

$obj = $this->db->query_update( $to_table_name, $data, $where );

Log::I( ">>> table [$from_table_name] proc..." );

$db->close();

Log::I( '<<<<<< END >>>>>>' );
Log::I( '=========================' );
?>