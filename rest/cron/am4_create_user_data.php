<?php
/*
 * 每日4:00执行。
 * 新建新的一天的统计表。
 */
require_once ( "../config.php" );
require_once ( DBASE_PATH . '/dbase.php' );
require_once ( PHPROOT . "/inc/Log.php" );
require_once ( PHPROOT . "/inc/func.php" );

Log::I( '=========================' );
Log::I( __FILE__ );

Log::I( '<<<<<< START >>>>>>' );

$date_table = 'user_data';
$date_table_newday = $date_table . '_' . get_Ymd();
$date_table_yestoday = $date_table . '_' . get_yestoday_Ymd();

$db = new Database();
$db->connect();

// /////////////////////////////////////
// 新建一张每日记录的表。
Log::I( ">>> table [$date_table_newday] proc..." );
$sql = "drop table if exists $date_table_newday;";
$obj = $db->query( $sql );

$sql = "create table $date_table_newday like $date_table;";
$obj = $db->query( $sql );

// /////////////////////////////////////

$db->close();

Log::I( '<<<<<< END >>>>>>' );
Log::I( '=========================' );
?>
