<?php
/*
 * 每日4:00执行。
 * 新建新的一天的统计表。
 */
require_once ( "../config.php" );
require_once ( DBASE_PATH . '/dbase.php' );
require_once ( PHPROOT . "/inc/Log.php" );
require_once ( PHPROOT . "/inc/func.php" );

Log::I( '==== statistics by 4 ====' );
Log::I( '<<<<<< START >>>>>>' );

$date_table = 'user_data';
$date_table_newday = $date_table . '_' . get_Ymd();
$date_table_yestoday = $table_name . '_' . get_yestoday_Ymd();

$db = new Database();
$db->connect();

// /////////////////////////////////////
// 新建一张每日记录的表。
Log::I( ">>> table [$date_table_newday] proc..." );
$sql = "drop table if exists $date_table_newday;";
$obj = $db->query( $sql );

$sql = "create table $date_table_newday like $date_table;";
$obj = $db->query( $sql );

// /////////////////////////////////////
// 对上日记录的信息进行统计。
// @user_data_by_user 每个用户累积统计
$table = TABLE_USER_DATA_BY_USER;

$sql = "select * from $date_table_yestoday;";
$obj = $db->fetch_all_array( $sql );
if ( is_valid_result( $obj ) ) {
    foreach ( $obj as $record ) {
        if ( is_valid_result( $record ) ) {
            $user_id = $record['user_id'];
            $platform_id = $record['platform_id'];
            
            Log::I("$user_id:$platform_id ...");
            
            $sql = "select user_id from $table where user_id = $user_id and platform_id = $platform_id";
            $user_data = $db->query( $sql );
            if ( is_valid_result( $user_data ) ) {
                // 老用戶。
                $update_data = array (
                        'last_update_date' => $record['last_update_date'],
                        'online_amount_time' => $user_data['online_amount_time'] + $record['online_amount_time'],
                        'login_amount' => $user_data['login_amount'] + $record['login_amount'],
                        'connect_amount' => $user_data['connect_amount'] + $record['connect_amount'],
                        'day_amount' => $user_data['day_amount'] + 1,
                        'pay_flag' => $user_data['day_amount'] | $record['pay_flag'] 
                );
                if ( $user_data['online_max_time'] < $record['online_max_time'] ) {
                    $update_data['online_max_time'] = $record['online_max_time'];
                }

                $where = " user_id = $user_id AND platform_id = $platform_id";
                
                $obj = $this->db->query_update( $table, $update_data, $where );
            }
            else {
                // 新用戶。
                $insert_data = array (
                        'user_id' => $user_id,
                        'platform_id' => $platform_id,
                        'first_login_date' => $record['first_login_date'],
                        'last_update_date' => $record['last_update_date'],
                        'online_amount_time' => $record['online_amount_time'],
                        'online_max_time' => $record['online_max_time'],
                        'login_amount' => $record['login_amount'],
                        'connect_amount' => $record['connect_amount'],
                        'day_amount' => $record['day_amount'],
                        'pay_flag' => $record['pay_flag']
                );
                $obj = $this->db->query_insert( $table, $insert_data );
            }
        }
    }
}
else {
    Log::W( "SQL result is null : $sql" );
}


// @user_data_by_date 每日统计



// /////////////////////////////////////

$db->close();

Log::I( '<<<<<< END >>>>>>' );
Log::I( '=========================' );
?>
