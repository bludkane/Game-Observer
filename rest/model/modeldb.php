<?php

class modeldb
{
    //protected $table_name = '';
    protected $db;

    function __construct()
    {
    }

    protected function Begin()
    {
        $this->db = new Database();
        $this->db->connect();
    }

    protected function EndOK()
    {
        $this->db->close();
        
        return new response( array (
                'body' => 0 
        ) );
    }

    protected function EndErr( $msg )
    {
        $this->db->close();
        
        return new response( array (
                'body' => $msg 
        ) );
    }

}

?>