<?php

class userdb extends modeldb
{
    const TABLE_KEY_NAME = 'user_data_';
    protected $table_name = self::TABLE_KEY_NAME;
    protected $table_name_full;
    protected $table_name_full_backup;

    function __construct()
    {
        $this->table_name_full .= get_Ymd();
        $this->table_name_full_backup .= get_yestoday_Ymd();
    }

    protected function Begin()
    {
        parent::Begin();
        $this->CheckNewTable();
    }
    
    protected function CheckNewTable()
    {
        $hour = date( "H" );
        $minute = date( "i" );
        if ( $hour == DATE_START_CLOCK && ( 0 < $minute && $minute < 10 ) ) {
            $obj = $db->query( "show tables like $table_name_full" );
            if ( isset($obj) && $db->get_num_rows() < 1 ) {
                Log::D( 'new table is not exist.table_name:' . $this->table_name_full );
                $this->table_name_full = $this->$table_name_full_backup;
            }
        }
    }
    

////////////////////////////////////
    function create( $user_id, $platform_id )
    {
        $this->Begin();
        
        $data = array (
                'user_id' => $user_id,
                'platform_id' => $platform_id,
                'new_flag' => 1 
        );
        $obj = $this->db->query_insert( $this->table_name_full, $data );
        
        return $this->EndOK();
    }

    function connect( $user_id, $platform_id )
    {
        $this->Begin();
        
        $sql = 'SELECT * FROM ' . $this->table_name_full . " WHERE user_id = $user_id AND platform_id = $platform_id";
        $obj = $this->db->query_first( $sql );
        if ( isset($obj) && !empty( $obj ) ) {
            
            Log::D( "111" );
            
            $now = time();
            $last_update_date = strtotime( $obj[ 'last_update_date' ] );
            $last_login_date = strtotime( $obj[ 'last_login_date' ] );

            Log::D( date( 'Y-m-d H:i:s', $now ) );
            Log::D( $obj[ 'last_update_date' ] );
            
            if ( $now - $last_update_date <= 1 ) {
                Log::W( "Connected to the last time is too short.user_id:$user_id,now:$now,last_update_date:$last_update_date" );
                return $this->EndOK();
            }
            
            $update_data = array (
                    'last_update_date' => 'NOW()',
                    'connect_amount' => $obj[ 'connect_amount' ] + 1 
            );
            
            if ( $now - $last_update_date > USER_LOGIN_TIMEOUT ) {
                $update_data[ 'last_login_date' ] = date( 'Y-m-d H:i:s', $now );
                $update_data[ 'login_amount' ] = $obj[ 'login_amount' ] + 1;
            }
            else {
                $update_data[ 'online_amount_time' ] = $obj[ 'online_amount_time' ] + ( $now - $last_update_date ) / 60;
                
                $online_time = ( $now - $last_login_date ) / 60 + 1;
                if ( $online_time > $obj[ 'online_max_time' ] ) {
                    $update_data[ 'online_max_time' ] = $online_time;
                }
            }
            
            $where = " user_id = $user_id AND platform_id = $platform_id";
            
            $obj = $this->db->query_update( $this->table_name_full, $update_data, $where );
        }
        else {
            
            Log::D( "222" );
            $sql = 'SELECT * FROM ' . TABLE_USER_DATA_BY_USER . " WHERE user_id = $user_id AND platform_id = $platform_id";
            $obj = $this->db->query_first( $sql );
            if ( isset($obj) && !empty( $obj ) ) {
                
                Log::D( "233" );
                $insert_data = array (
                        'user_id' => $user_id,
                        'platform_id' => $platform_id,
                        'pay_flag' => $obj[ 'pay_flag' ] 
                );
                $obj = $this->db->query_insert( $this->table_name_full, $insert_data );
            }
            else {
                Log::D( "244" );
                $insert_data = array (
                        'user_id' => $user_id,
                        'platform_id' => $platform_id,
                        'new_flag' => 1 
                );
                $obj = $this->db->query_insert( $this->table_name_full, $insert_data );
            }
        }
        
        return $this->EndOK();
    }

    function pay( $user_id, $platform_id )
    {
        $this->Begin();
        
        $sql = 'SELECT * FROM ' . $this->table_name_full . " WHERE user_id = $user_id AND platform_id = $platform_id";
        $obj = $this->db->query_first( $sql );
        if ( isset($obj) && !empty( $obj ) ) {
            $update_data = array (
                    'pay_flag' => 1 
            );

            $where = " user_id = $user_id AND platform_id = $platform_id";
            
            $obj = $this->db->query_update( $this->table_name_full, $update_data, $where );
        }
        else {
            $insert_data = array (
                    'user_id' => $user_id,
                    'platform_id' => $platform_id,
                    'pay_flag' => 1 
            );
            $obj = $this->db->query_insert( $this->table_name_full, $insert_data );
        }
        
        return $this->EndOK();
    }

}

?>